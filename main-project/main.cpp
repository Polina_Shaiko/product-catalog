#include <iostream>

using namespace std;

#include "products.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"


int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �10. ������� �������\n";
    cout << "�����: ����� ������\n\n";
    product_catalog* products[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", products, size);
        cout << "***** ������� ������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /******** ����� ��������� *********/
            cout << "���������...: ";
            cout << products[i]->cost << '\n';
            /******** ����� ���������� ********/
            cout << "����������...: ";
            cout << products[i]->kol << '\n';
            /******** ����� ��������� ********/
            cout << "���������...: ";
            cout << products[i]->category << '\n';
            /******** ����� �������� ********/
            cout << "��������...: ";
            cout << products[i]->name << '\n';
            cout << '\n';
        }

        bool (*check_function)(product_catalog*) = NULL; // check_function - ��� ��������� �� �������, ������������ �������� ���� bool,
                                                          // � ����������� � �������� ��������� �������� ���� book_subscription*
        cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
        cout << "1) ������� ��� ������ � ��������� ������������.\n";
        cout << "2) ������� ��� ������ ���������� ���� 100 ������.\n";
        cout << "\n������� ����� ���������� ������: ";
        int item;
        cin >> item;
        cout << '\n';

        switch (item)
        {
        case 1:
            check_function = check_results_by_category; // ����������� � ��������� �� ������� ��������������� �������
            cout << "***** ������� ��� ������ � ��������� ������������ *****\n\n";
            break;
        case 2:
            check_function = check_results_by_price; // ����������� � ��������� �� ������� ��������������� �������
            cout << "***** ������� ��� ������ ���������� ���� 100 ������ *****\n\n";
            break;
        default:
            throw "������������ ����� ������";
        }
        
            if (check_function) {

                int new_size;
                product_catalog** filtered = filter(products, size, check_function, new_size);
                for (int i = 0; i < new_size; i++)
                {
                    /******** ����� ��������� *********/
                    cout << "���������...: ";
                    cout << filtered[i]->cost << '\n';
                    /******** ����� ���������� ********/
                    cout << "����������...: ";
                    cout << filtered[i]->kol << '\n';
                    /******** ����� ��������� ********/
                    cout << "���������...: ";
                    cout << filtered[i]->category << '\n';
                    /******** ����� �������� ********/
                    cout << "��������...: ";
                    cout << filtered[i]->name << '\n';
                    cout << '\n';
                }
                delete[] filtered;
            }

            for (int i = 0; i < size; i++)
            {
                delete products[i];
            }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}
