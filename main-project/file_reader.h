#ifndef FILE_READER_H
#define FILE_READER_H

#include "products.h"

void read(const char* file_name, product_catalog* array[], int& size);

#endif